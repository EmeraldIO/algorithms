package com.example;

public class Main {
    public static void main(String[] args) {
        int[] myList = {1,3,5,7,9};

        System.out.println(binary_search(myList,7));
    }

    public static int binary_search(int[] array, int item){
        int low = 0;
        int high = array.length-1;

        while (low<=high){
            int mid = (low+high)/2;
            int guess = array[mid];
            if(guess==item){
                return mid;
            }
            if(guess>item){
                high = mid-1;
            }
            else{
                low = mid+1;
            }
        }
        return 0;
    }
}
