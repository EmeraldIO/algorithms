package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) { //direct implementation from the book
        int[] testArray = {5,3,6,2,10};
        System.out.println(Arrays.toString(selectionSort(testArray)));
    }

    public static int findSmallest(List<Integer> arr){
        int smallest = arr.get(0);
        int smallestIndex = 0;
        for (int i = 1; i <arr.size(); i++) {
            if(arr.get(i)<smallest){
                smallest = arr.get(i);
                smallestIndex = i;
            }
        }

        return smallestIndex;
    }

    public static int[] selectionSort(int[] array){

        List<Integer> arrayList =new ArrayList<Integer>();
        for (int i = 0; i <array.length ; i++) {
            arrayList.add(array[i]);
        }
        int[] finalArray=new int[array.length];
        for (int i = 0; i <array.length ; i++) {
            int smallest = findSmallest(arrayList);
            finalArray[i]= (int) arrayList.get(smallest);
            arrayList.remove((int)smallest);
        }
        return finalArray;
    }
}
